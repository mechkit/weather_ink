from datetime import datetime
import json
from PIL import Image, ImageDraw, ImageFont
from adafruit_epd.epd import Adafruit_EPD

forcasts_font = ImageFont.truetype(
    "/usr/share/fonts/truetype/dejavu/DejaVuSans-Bold.ttf", 12
)
small_font = ImageFont.truetype(
    "/usr/share/fonts/truetype/dejavu/DejaVuSans-Bold.ttf", 16
)
medium_font = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf", 20)
large_font = ImageFont.truetype(
    "/usr/share/fonts/truetype/dejavu/DejaVuSans-Bold.ttf", 24
)
icon_font = ImageFont.truetype("./meteocons.ttf", 14)

# Map the OpenWeatherMap icon code to the appropriate font character
# See http://www.alessioatzeni.com/meteocons/ for icons
ICON_MAP = {
    "01d": "B",
    "01n": "C",
    "02d": "H",
    "02n": "I",
    "03d": "N",
    "03n": "N",
    "04d": "Y",
    "04n": "Y",
    "09d": "Q",
    "09n": "Q",
    "10d": "R",
    "10n": "R",
    "11d": "Z",
    "11n": "Z",
    "13d": "W",
    "13n": "W",
    "50d": "J",
    "50n": "K",
}

# RGB Colors
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)


class Weather_Graphics:
    def __init__(self, display, *, am_pm=True, celsius=True):
        self.am_pm = am_pm
        self.celsius = celsius

        self.forcasts_font = forcasts_font
        self.small_font = small_font
        self.medium_font = medium_font
        self.large_font = large_font

        self.display = display

        self._weather_icon = None
        self._city_name = None
        self._main_text = None
        self._temperature = None
        self._description = None
        self._time_text = None

    def display_weather(self, weather):
        weather = json.loads(weather.decode("utf-8"))

        self.forcasts = []
        self.forcasts_icons = []
        for weather_event in weather['list']:
            hour = datetime.fromtimestamp(weather_event['dt']).strftime('%I')
            if hour in ['02','08']:
                time = datetime.fromtimestamp(weather_event['dt']).strftime('%a %I %p')
                temperature = "{:3.0f}".format( weather_event['main']['temp'])
                clouds = weather_event['weather'][0]['description']
                self.forcasts.append( time + " " + temperature + "°F " + clouds )
                cloud_icon = ICON_MAP[weather_event["weather"][0]["icon"]]
                self.forcasts_icons.append( cloud_icon )

        """
        # set the icon/background
        self._weather_icon = ICON_MAP[weather["weather"][0]["icon"]]

        city_name = weather["name"] + ", " + weather["sys"]["country"]
        print(city_name)
        self._city_name = city_name

        main = weather["weather"][0]["main"]
        print(main)
        self._main_text = main

        temperature = weather["main"]["temp"] - 273.15  # its...in kelvin
        print(temperature)
        if self.celsius:
            self._temperature = "%d °C" % temperature
        else:
            self._temperature = "%d °F" % ((temperature * 9 / 5) + 32)

        description = weather["weather"][0]["description"]
        description = description[0].upper() + description[1:]
        print(description)
        self._description = description
        # "thunderstorm with heavy drizzle"
        """
        self.update_time()

    def update_time(self):
        now = datetime.now()
        #self._time_text = now.strftime("%I:%M %p").lstrip("0").replace(" 0", " ")
        self._time_text = 'Updated: ' + now.strftime('%a %I:%M %p')

        self.update_display()

    def update_display(self):
        #self.display.fill(Adafruit_EPD.WHITE)
        self.display = {
                'width': 250,
                'height': 122,
        }
        image = Image.new("RGB", (self.display['width'], self.display['height']), color=WHITE)
        draw = ImageDraw.Draw(image)

        x = 1
        y = 1

        # Draw the time
        (font_width, font_height) = forcasts_font.getsize(self._time_text)
        draw.text(
            (
                x,
                y,
            ),
            self._time_text,
            font=self.forcasts_font,
            fill=BLACK,
        )
        y += font_height + 1

        for i,forcast in enumerate(self.forcasts):
            icon = self.forcasts_icons[i]
            # Draw the Icon
            (font_width, font_height) = icon_font.getsize(icon)
            draw.text(
                (x,y),
                icon,
                font=icon_font,
                fill=BLACK,
            )
            draw.text(
                (
                    x + font_width + 1 ,
                    y,
                ), forcast, font=self.forcasts_font, fill=BLACK,
            )
            y += font_height + 1


        # save a image using extension
        image = image.save("weather.jpg")
        #self.display.image(image)
        #self.display.display()
