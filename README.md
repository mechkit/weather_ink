# Weather Ink

Based on [Raspberry Pi E-Ink Weather Station using Python | Adafruit Learning System](https://learn.adafruit.com/raspberry-pi-e-ink-weather-station-using-python/overview)

...for the [Adafruit 2.13 Monochrome E-Ink Bonnet for Raspberry Pi [THINK INK] : ID 4687 : $19.95 : Adafruit Industries, Unique & fun DIY electronics and kits](https://www.adafruit.com/product/4687)
